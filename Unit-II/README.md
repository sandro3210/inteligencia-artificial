# Búsqueda por anchura

El algoritmo calcula todos los movimientos posibles en el juego y los almacena de manera
ordenada en un árbol, expandiendo cada estado por nivel o por prundidad.

Posterior a eso y una vez encontrado el estado solución retorna un arreglo con los
estados y el movimiento necesario para alcanzarlo.

### Referencias

- [Tree](https://en.wikipedia.org/wiki/Tree_(data_structure))
- [Breadth-first search](https://en.wikipedia.org/wiki/Breadth-first_search#Pseudocode)
- [Depth-first search](https://en.wikipedia.org/wiki/Depth-first_search#Pseudocode)
