import pygame
from tictactoe import Tictactoe as ttt
from minimax import *
from map_line import map_line


# Globals
WIDTH = HEIGHT = 720
FPS = 60
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)

# Game settings
board_size = 3
ai_turn = 'O' # X starts
ai_depth = 4

# Pygame settings
gap = WIDTH // board_size
pygame.init()
screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Tic Tac Toe")
clock = pygame.time.Clock()
pause = False
done = False

# TicTacToe instance
game = ttt(size=board_size)
winner = ''

# Game sprites
x_logo = pygame.image.load('img/x.png')
o_logo = pygame.image.load('img/o.png')
x_logo = pygame.transform.scale(x_logo, (gap, gap))
o_logo = pygame.transform.scale(o_logo, (gap, gap))

x_pos, o_pos = [], []

# Game fonts
message = pygame.font.Font('freesansbold.ttf', 60)
esc_menu = pygame.font.Font('freesansbold.ttf', 30)


def restart():

    global winner

    x_pos[:], o_pos[:] = [], []
    game.reset()
    del(winner)
    winner = ''


def player_movement(x, y):
    
    global winner

    valid_move = game.next_move(*board_pos)

    if valid_move:
        if not ai_turn == 'X':
            x_pos.append((board_pos[0] * gap, board_pos[1] * gap))
        else:
            o_pos.append((board_pos[0] * gap, board_pos[1] * gap))

    winner = game.winner()


def ai_movement():

    global winner

    board_pos = ai_selection(game)
    valid_move = game.next_move(*board_pos)

    if valid_move:
        if ai_turn == 'X':
            x_pos.append((board_pos[0] * gap, board_pos[1] * gap))
        else:
            o_pos.append((board_pos[0] * gap, board_pos[1] * gap))

    winner = game.winner()


def draw(screen):

    # Background
    screen.fill(BLACK)

    # Lines
    for i in range(1, board_size):
        # Vertical
        pygame.draw.line(
            screen, WHITE,
            (int(i * gap), 0), (int(i * gap), HEIGHT), 3
        )

        # Horizontal
        pygame.draw.line(
            screen, WHITE,
            (0, int(i * gap)), (WIDTH, int(i * gap)), 3
        )

    # Simbols
    for x in x_pos:
        screen.blit(x_logo, x)
    
    for o in o_pos:
        screen.blit(o_logo, o)

    if winner:
        if winner == 'X':
            winner_message = message.render("X Won!", True, WHITE)
        elif winner == 'O':
            winner_message = message.render("O Won!", True, WHITE)
        elif winner == 'T':
            winner_message = message.render("Tie!", True, WHITE)
        
        winner_message_rect = winner_message.get_rect(center=(WIDTH//2, HEIGHT//2))
        screen.blit(winner_message, winner_message_rect)

        end_message = esc_menu.render('Press "ESC" to quit', True, WHITE)
        end_message_rect = end_message.get_rect(center=(WIDTH//2, HEIGHT//2 + 50))
        screen.blit(end_message, end_message_rect)


# Main loop
while not done:
    board_pos = None

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                done = True

            if event.key == pygame.K_r:
                restart()

        if event.type == pygame.MOUSEBUTTONUP:
            mouse_pos = pygame.mouse.get_pos()
            board_pos = (
                int(map_line(mouse_pos[0], *(0, WIDTH), *(0, board_size))),
                int(map_line(mouse_pos[1], *(0, HEIGHT), *(0, board_size)))
            )

    #print(game.get_turn())
    if game.get_turn() == ai_turn:
        ai_movement()
    elif board_pos:
        player_movement(*board_pos)

    draw(screen)

    clock.tick(FPS)
    pygame.display.update()
